# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include audacity::install
class audacity::install {
  package { $audacity::package_name:
    ensure => $audacity::package_ensure,
  }
}
